// users-model.js - A mongoose model
//
// See http://mongoosejs.com/docs/models.html
// for more of what you can do here.
const validator = require('validator');
module.exports = function (app) {
  const mongooseClient = app.get('mongooseClient');
  const users = new mongooseClient.Schema({
  
    email: {
      type: String, 
      unique: true, 
      required: [true, 'User email is required'],
      validate: {
        validator: val => {
          return validator.isEmail(val);
        },
        message: props => `'${props.value}' is not email`
      }
    },
    password: {
      type: String,
      required: [true, 'Unable to create a user without password']
    },
  
  
  }, {
    timestamps: true
  });

  return mongooseClient.model('users', users);
};
