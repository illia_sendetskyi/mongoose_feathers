const mongoose = require('mongoose');
const logger = require('./logger');

const options = {
  useNewUrlParser: true
};
module.exports = function (app) {
  mongoose.connect(app.get('mongodb'), options).catch(e => {
    logger.error(e);
  });
  mongoose.Promise = global.Promise;

  app.set('mongooseClient', mongoose);
};
