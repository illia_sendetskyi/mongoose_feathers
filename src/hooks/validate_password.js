// Use this hook to manipulate incoming or outgoing data.
// For more information on hooks see: http://docs.feathersjs.com/api/hooks.html
const errors = require('@feathersjs/errors');

// eslint-disable-next-line no-unused-vars
module.exports = function (options = {}) {
  return async context => {
    if (context.data.password.length < 6) {
      return Promise.reject(new errors.BadRequest('Too short password. It must be longer 6 symbols'));
    }
    return undefined;
  };
};