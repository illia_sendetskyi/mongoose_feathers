const { createLogger, format, transports } = require('winston');

// Configure the Winston logger. For the complete documentation see https://github.com/winstonjs/winston
const path = require('path');
const logger = createLogger({
  // To see more detailed errors, change this to 'debug'
  level: 'info',
  format: format.combine(
    format.timestamp(),
    format.splat(),
    format.simple(),
  ),
  transports: [
    new transports.Console(),
    new transports.File({ filename: `${path}/../data/logs/error.log`, level: 'error' }),
    new transports.File({ filename: `${path}/../data/logs/combined.log` })
  ],
});

module.exports = logger;
